<?php
/**
 * Correspondence
 *
 * @Entity
 * @Table(name="correspondence_history")
 *
 */
class App_Model_CorrespondenceHistory {

	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var string
	 *
	 * @Column(name="status", type="string", length=100, nullable=true)
	 */
	private $_status;
	/**
	 * @var datetime
	 *
	 * @Column(name="date_log", type="datetime", nullable=true)
	 */
	private $_dateLog;
	/**
	 * @var App_Model_Correspondence
	 *
	 * @ManyToOne(targetEntity="App_Model_Correspondence", cascade={"persist"})
	 * @JoinColumn(name="correspondence_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_correspondence;
	
	public function __construct($correspondence, $status) {
		$this->_status = $status;
		$this->_dateLog = new DateTime();
		$this->_correspondence = $correspondence;
	}
	
	public function getStatus() {
		return $this->_status;
	}

	public function getDateLog() {
		return $this->_dateLog;
	}
	/**
	 * @return App_Model_Correspondence
	 */
	public function getCorrespondence() {
		return $this->_correspondence;
	}
	public static function getAll(){
		$dao = new App_Dao_CorrespondenceHistoryDao();
		return $dao->getAll();		
	}
	public static function getAllByStatusAndDateRange($status,$dateFrom,$dateTo){
		$dao = new App_Dao_CorrespondenceHistoryDao();
		return $dao->getAllByStatusAndDateRange($status,$dateFrom,$dateTo);		
	}
	
	public function save() {
		$dao = new App_Dao_CorrespondenceHistoryDao();
		$dao->save($this);
	}
	
}
