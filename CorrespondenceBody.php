<?php
/**
 * CorrespondenceBody
 *
 * @Entity
 * @Table(name="correspondence_body")
 *
 */
class App_Model_CorrespondenceBody {
	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var string
	 *
	 * @Column(name="controlCode", type="string", length=100, nullable=true)
	 */
	private $_controlCode;
	/**
	 *@var App_Model_DocumentType
	 *
	 * @ManyToOne(targetEntity="App_Model_DocumentType")
	 * @JoinColumn(name="document_type_id", referencedColumnName="id")
	 *
	 **/
	private $_documentType;
	/**
	 * @var string
	 *
	 * @Column(name="subject", type="string", length=100, nullable=true)
	 */
	private $_subject;
	/**
	 * @var string
	 *
	 * @Column(name="file_path", type="string", length=100, nullable=true)
	 */
	private $_filePath;

	public function __construct($subject, $documentType) {
		$this->_subject = $subject;
		$this->_documentType = $documentType;
	}

	public function getId() {
		return $this->_id;
	}

	public function getControlCode() {
		return $this->_controlCode;
	}

	public function getDocumentType() {
		return $this->_documentType;
	}

	public function getSubject() {
		return $this->_subject;
	}

	public function getFilePath() {
		return $this->_filePath;
	}

	public function changeSubject($subject) {
		$this->_subject = $subject;
	}

	public function changeFilePath($filePath) {
		$this->_filePath = $filePath;
	}

	public function toString() {
		$result = "{";
		$result .= "\"id\" : " . $this->_id . ",";
		$result .= "\"controlCode\" : " . $controlCode . ",";
		$result .= "\"documentType\" : " . $this->_documentType->getId() . ",";
		$result .= "\"subject\" : " . $this->_subject . ",";
		$result .= "\"filePath\" : " . $this->_filePath . ",";
		$result .= "}";
		return $result;
	}

	public function generateControlCode() {
		if ($this->_controlCode !== null) {
			throw new Exception('Solo se puede generar el codigo control una sola vez');
		}
		$type = ($this->_documentType->getType() == App_Model_DocumentType::EXTERNAL) ? "EX" : "IN";
		$dateTime = new Zend_Date();
		$date = $dateTime->toArray();
		$codeResult = $type;
		$codeResult .= "-";
		$codeResult .= $this->_documentType->getCode();
		$codeResult .= "-";
		$codeResult .= $date["year"] . "-" . $date["month"];
		$codeResult .= "-";
		$codeResult .= $this->_documentType->getActualNumber();
		$this->_documentType->incrementActualNumber();

		$this->_controlCode = $codeResult;
	}

	public function save() {
		$dao = new App_Dao_CorrespondenceBodyDao();
		$dao->save($this);
	}

	public function remove() {
		$dao = new App_Dao_CorrespondenceBodyDao();
		$dao->remove($this);
	}

	public static function getById($id) {
		$dao = new App_Dao_CorrespondenceBodyDao();
		return $dao->getById($id);
	}

}
