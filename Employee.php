<?php
/**
 *
 * @Entity
 *
 */
class App_Model_Employee extends App_Model_EndPoint {
	
	const STATUS_ACTIVE = 'active';
	const STATUS_INACTIVE = 'inactive';

	/**
	 * @var App_Model_User
	 * @ManyToOne(targetEntity="App_Model_User")
	 * @JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
	 *
	 */
	private $_user;
	
	/**
	 * @var string
	 *
	 * @Column(name="status", type="string", length=100, nullable=false)
	 */
	private $_status;
	
	/**
	 * @var App_Model_CompanyArea
	 *
	 * @ManyToOne(targetEntity="App_Model_CompanyArea")
	 * @JoinColumn(name="company_area_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_companyArea;
	

	public function __construct($name, $lastName) {
		parent::__construct($name, $lastName);
		$this->_status = static::STATUS_ACTIVE;
		// $this->_user = new App_Model_User($login, $password);
	}
	
	public function inactivate() {
		$this->_status = static::STATUS_INACTIVE;
	}
	
	public function activate() {
		$this->_status = static::STATUS_ACTIVE;
	}
	
	public function setUser($user) {
		$this->_user = $user;
	}
	
	public function setCompanyArea($companyArea) {
		$this->_companyArea = $companyArea;
	}
	
	/**
	 * @return App_Model_CompanyArea
	 */
	public function getCompanyArea() {
		return $this->_companyArea;
	}
	
	/**
	 * @return App_Model_User
	 */
	public function getUser() {
		return $this->_user;
	}

	public function getStatus() {
		return $this->_status;
	}

	public function changeUser($user) {
		$this->_user = $user;
	}
	
	public function __toString() {
		$string = "Employee: {";
		$string = $string . "<br />id: " . parent::$_id;
		$string = $string . "<br />employeeName: " . $this->_name;
		$string = $string . "<br />employeeLastName: " . $this->_lastName;
		$string = $string . "<br />usuario: " . $this->_user;
		$string = $string . "<br />estado: " . $this->_status;
		$string = $string . "<br />}";
		return $string;
	}

	public static function getAll($limit, $offset) {
		$dao = new App_Dao_EmployeeDao();
		return $dao->getAll($limit, $offset);
	}
	
	public static function getAllWithCompanyArea($limit, $offset) {
		$dao = new App_Dao_EmployeeDao();
		return $dao->getAllWithCompanyArea($limit, $offset);
	}
	
	public static function getById($id) {
		$dao = new App_Dao_EmployeeDao();
		return $dao->getById($id);
	}
	
	/**
	 * @return App_Model_Employee
	 */
	public static function getByUserId($userId) {
		$dao = new App_Dao_EmployeeDao();
		return $dao->getByUserId($userId);
	}
	
	public function save() {
		$dao = new App_Dao_EmployeeDao();
		$dao->save($this);
	}
	
	public function remove() {
		$dao = new App_Dao_EmployeeDao();
		$dao->remove($this);
	}
	
	public function getCorrespondenceToSend($limit, $offset) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getToSendByOwnerId($this->_ownerId, $limit, $offset);
	}
	
	public function getReceivedCorrespondence($limit, $offset) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getReceivedByOwnerId($this->_ownerId, $limit, $offset);
	}
	
	public function getSentCorrespondence($limit, $offset) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getSentByOwnerId($this->_ownerId, $limit, $offset);
	}
}
