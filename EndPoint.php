<?php
/**
 * @Entity
 * @Table(name="end_point")
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({"endPoint" = "App_Model_EndPoint", "employee" = "App_Model_Employee", "externalPerson" = "App_Model_ExternalPerson"})
 *
 */
class App_Model_EndPoint {
	/**
	 * @var integer
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	protected $_id;

	/**
	 * @var string
	 *
	 * @Column(name="first_name", type="string", nullable=false)
	 */
	protected $_name;

	/**
	 * @var string
	 *
	 * @Column(name="last_name", type="string", nullable=false)
	 */
	protected $_lastName;

	public function __construct($name, $lastName) {
		$this->_name = $name;
		$this->_lastName = $lastName;
	}

	public function getId() {
		return $this->_id;
	}

	public function getName() {
		return $this->_name;
	}

	public function getLastName() {
		return $this->_lastName;
	}

	public function setName($name) {
		$this->_name = $name;
	}

	public function setLastName($lastName) {
		$this->_lastName = $lastName;
	}
	
	public function getFullName() {
		return $this->_name." ". $this->_lastName;
	}
	
}
