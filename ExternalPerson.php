<?php
/**
 * @Entity
 */
class App_Model_ExternalPerson extends App_Model_EndPoint {
    /**
     * @var boolean
     *
     * @Column(name="is_company", type="boolean", nullable=true)
     */
    private $_isCompany;
    /**
     * @var string
     *
     * @column(name="company_name", type="string", nullable=true)
     */
    private $_companyName;

    public function __construct($name, $lastName) {
        parent::__construct($name, $lastName);
    }

    public function isCompany() {
        $isCompany = false;
        if ($this->_companyName != null) {
            $isCompany = true;
        }
        return $isCompany;
    }

    public function setCompanyName($companyName) {
        $this->_companyName = $companyName;
    }

    public function getCompanyName() {
        return $this->_companyName;
    }
	
	public function changeName($newName) {
		$this->_name = $newName;
	}
	
	public function changeLastName($newLastName) {
		$this->_lastName = $newLastName;
	}
	
	public static function getById($id) {
		$dao = new App_Dao_ExternalPersonDao();
		return $dao->getById($id);
	}
	
	public static function getAll($limit, $offset) {
		$dao = new App_Dao_ExternalPersonDao();
		return $dao->getAll($limit, $offset);
	}
	
	public function save() {
		$dao = new App_Dao_ExternalPersonDao();
		return $dao->save($this);
	}
	
	public function remove() {
		$dao = new App_Dao_ExternalPersonDao();
		return $dao->remove($this);
	}

    public function __toString() {
        $string = "External_Person: {";
        $string = $string . "<br />id: " . parent::$_id;
        $string = $string . "<br />Name: " . parent::$_name;
        $string = $string . "<br />LastName: " . parent::$_lastName;
        $string = $string . "<br />companyName: " . $this->_companyName;
        $string = $string . "<br />}";
        return $string;
    }

}
