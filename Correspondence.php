<?php
/**
 * Correspondence
 *
 * @Entity
 * @Table(name="correspondence")
 *
 */
class App_Model_Correspondence {
	const STATUS_WAITING = 'waiting';
	const STATUS_TO_SEND = 'to_send';
	const STATUS_SENT = 'sent';
	const STATUS_RECEIVED = 'received';
	const STATUS_DELIVERED = 'delivered';
	const STATUS_TO_WAIT = 'to_wait';
	const STATUS_TO_RESEND = 'to_resend';
	const STATUS_RESENT = 'resent';
	const STATUS_ARCHIVED = 'archived';
	const STATUS_DELETED = 'deleted';

	const ORIGIN_INTERNAL = "internal";
	const ORIGIN_EXTERNAL = "external";

	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;

	/**
	 *@var App_Model_EndPoint
	 *
	 * @ManyToOne(targetEntity="App_Model_EndPoint")
	 * @JoinColumn(name="owner_id", referencedColumnName="id")
	 *
	 **/
	private $_owner;

	/**
	 *@var App_Model_Correspondence
	 *
	 * @ManyToOne(targetEntity="App_Model_Correspondence")
	 * @JoinColumn(name="sender_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_sender;
	/**
	 * @var integer
	 *
	 * @Column(name="level", type="integer", nullable=true)
	 *
	 */
	private $_level;
	/**
	 * @var string
	 *
	 * @Column(name="message", type="string", length=100, nullable=true)
	 */
	private $_message;
	/**
	 *
	 * @OneToMany(targetEntity="App_Model_AssignedAction", mappedBy="_correspondence", cascade={"all"})
	 */
	private $_assignedActions;
	/**
	 * @var string
	 *
	 * @Column(name="status", type="string", length=100, nullable=true)
	 */
	private $_status;
	
	/**
	 * @var string
	 *
	 * @Column(name="previous_status", type="string", length=100, nullable=true)
	 */
	private $_previousStatus;
	/**
	 * @ManyToOne(targetEntity="App_Model_CorrespondenceBody")
	 * @JoinColumn(name="correspondence_body_id", referencedColumnName="id")
	 **/
	private $_correspondenceBody;

	/**
	 * @OneToMany(targetEntity="App_Model_Correspondence", mappedBy="_sender", cascade={"all"})
	 */
	private $_receivers;

	/**
	 * @var string
	 *
	 * @Column(name="origin", type="string", length=100, nullable=true)
	 */
	private $_origin;

	/**
	 * @var DateTime
	 *
	 * @Column(name="date_received", type="datetime", nullable=true)
	 */
	private $_dateReceived;
	/**
	 * @var App_Model_CorrespondenceHistory[]
	 * @OneToMany(targetEntity="App_Model_CorrespondenceHistory", mappedBy="_correspondence", cascade={"all"})
	 */
	private $_correspondenceHistory;

	public function __construct(App_Model_EndPoint $owner, App_Model_CorrespondenceBody $correspondenceBody, $origin) {
		$this->_actions = array();
		$this->_receivers = array();
		$this->_owner = $owner;
		$this->_correspondenceBody = $correspondenceBody;
		$this->_origin = $origin;
	}
	
	public function getCorrespondenceHistory() {
		return $this->_correspondenceHistory;
	}

	public function getId() {
		return $this->_id;
	}

	public function getStatus() {
		return $this->_status;
	}
	
	public function getPreviousStatus() {
		return $this->_previousStatus;
	}
	
	public function getOwner() {
		return $this->_owner;
	}
	
	public function getSender() {
		return $this->_sender;
	}

	public function getLevel() {
		return $this->_level;
	}

	public function getMessage() {
		return $this->_message;
	}

	public function getAssignedActions() {
		return $this->_assignedActions;
	}

	public function getControlCode() {
		if (isset($this->_correspondenceBody)) {
			return $this->_correspondenceBody->getControlCode();
		}
	}

	public function getDocumentType() {
		if (isset($this->_correspondenceBody)) {
			return $this->_correspondenceBody->getDocumentType();
		}
	}

	public function getSubject() {
		if (isset($this->_correspondenceBody)) {
			return $this->_correspondenceBody->getSubject();
		}
	}

	public function getFilePath() {
		if (isset($this->_correspondenceBody)) {
			return $this->_correspondenceBody->getFilePath();
		}
	}
	
	public function getDateCreated() {
		return $this->_dateCreated;
	}

	public function getReceivers() {
		return $this->_receivers;
	}

	public function getOrigin() {
		return $this->_origin;
	}

	public function setSender(App_Model_Correspondence $sender) {
		$this->_sender = $sender;
	}

	public function changeOwner(App_Model_EndPoint $newOwner) {
		//TODO: Validar en que status se permite cambiar
		$this->_owner = $newOwner;
	}

	public function changeMessage($newMessage) {
		//TODO: Validar en que status se permite cambiar
		$this->_message = $newMessage;
	}

	public function changeAssignedActions($newAssignedActions) {
		//TODO: Validar en que status se permite cambiar
		$this->_assignedActions = $newAssignedActions;
		foreach ($newAssignedActions as $assignedAction) {
			$assignedAction->setCorrespondence($this);
		}
	}

	public function changeSubject($newSubject) {
		//TODO: Validar en que status se permite cambiar
		$this->_correspondenceBody->changeSubject($newSubject);
	}

	public function changeFilePath($newFilePath) {
		//TODO: Validar en que status se permite cambiar
		$this->_correspondenceBody->changeFilePath($newFilePath);
		$this->_correspondenceBody->save();
	}

	public function modify() {
		//TODO: Validar que no se guarden correspondencias que no han sido inicializadas aun.
		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($this);
	}

	public function remove() {
		$dao = new App_Dao_CorrespondenceDao();
		if ($this->_status === App_Model_Correspondence::STATUS_TO_WAIT || $this->_status === App_Model_Correspondence::STATUS_TO_SEND) {
			$dao->remove($this);
		} else {
			throw new Exception("No es posible eliminar.");
		}
	}

	/**
	 * @return App_Model_Correspondence
	 */
	public static function getById($id) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getById($id);
	}

	public static function getAllExternalToReceive($limit, $offset) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getAllExternalToReceive($limit, $offset);
	}

	public static function getAllToReceive($ownerId) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getToReceiveByOwnerId($ownerId, 1000, 0);
	}

	public static function getAllReceivedAndToReceive($ownerId) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getAllReceivedAndToReceiveByOwnerId($ownerId, 1000, 0);
	}

	public static function getAllSent($ownerId) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getSentByOwnerId($ownerId, 1000, 0);
	}
	
	public static function getAllArchived($ownerId) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getArchivedByOwnerId($ownerId, 1000, 0);
	}

	public function start() {
		if ($this->getId() !== null) {
			throw new Exception('Esta correspondencia ya fue inciada');
		}
		$this->_previousStatus = $this->_status;
		$this->_status = App_Model_Correspondence::STATUS_TO_SEND;
		$this->_correspondenceBody->generateControlCode();
		$this->_correspondenceBody->save();
		
		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($this);
		
		$history = new App_Model_CorrespondenceHistory($this, $this->getStatus());
		$history->save();
	}

	public function addAssignedAction(App_Model_AssignedAction $assignedAction) {
		$assignedAction->setCorrespondence($this);
		$this->_assignedActions[] = $assignedAction;
	}

	public function addReceiver(App_Model_EndPoint $owner, $assignedActions, $message) {
		if ($this->_status !== App_Model_Correspondence::STATUS_TO_SEND && $this->_status !== App_Model_Correspondence::STATUS_TO_RESEND) {
			throw new Exception('No se puede agregar destinatario');
		}

		// if (($owner instanceof App_Model_Employee) && ($this->_correspondenceBody->getDocumentType()->getType() !== App_Model_DocumentType::INTERNAL)) {
		// throw new Exception('No se puede juntar Employee con una correspondencia que no sea interna');
		// }

		if (($owner instanceof App_Model_ExternalPerson) && ($this->_correspondenceBody->getDocumentType()->getType() !== App_Model_DocumentType::EXTERNAL)) {
			throw new Exception('No se puede juntar External person con una correspondencia que no sea external');
		}

		$receiver = new App_Model_Correspondence($owner, $this->_correspondenceBody, $this->_origin);
		$receiver->changeMessage($message);
		foreach ($assignedActions as $assignedAction) {
			/**
			 * @var App_Model_AssignedAction
			 *
			 */
			$assignedAction;
			$receiver->addAssignedAction($assignedAction);

		}
		$receiver->prepareForReception();
		$receiver->setSender($this);

		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($receiver);
		
		$history = new App_Model_CorrespondenceHistory($receiver, $receiver->getStatus());
		$history->save();
	}

	public function send() {
		if ($this->_status !== App_Model_Correspondence::STATUS_TO_RESEND && $this->_status !== App_Model_Correspondence::STATUS_TO_SEND) {
			throw new Exception('No se puede enviar la correspondencia');
		}
		if ($this->_status === App_Model_Correspondence::STATUS_TO_RESEND) {
			$this->_previousStatus = $this->_status;
			$this->_status = App_Model_Correspondence::STATUS_RESENT;
		} elseif ($this->_status === App_Model_Correspondence::STATUS_TO_SEND) {
			$this->_previousStatus = $this->_status;
			$this->_status = App_Model_Correspondence::STATUS_SENT;
		} else {
			throw new Exception('No se pudo enviar la correspondencia 2');
		}

		foreach ($this->_receivers as $receiver) {
			/**
			 * @var App_Model_Correspondence
			 */
			$receiver;
			if ($receiver->getStatus() === App_Model_Correspondence::STATUS_TO_WAIT) {
				$receiver->receive();
			}
		}

		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($this);
		
		$history = new App_Model_CorrespondenceHistory($this, $this->getStatus());
		$history->save();
	}

	public function prepareForReception() {
		if ($this->_status !== null) {
			throw new Exception("No se puede preparar la recepcion del destinatario");
		}
		$this->_previousStatus = $this->_status;
		$this->_status = App_Model_Correspondence::STATUS_TO_WAIT;
	}

	public function receive() {
		if ($this->_status !== App_Model_Correspondence::STATUS_TO_WAIT) {
			throw new Exception('No se puede preparar la recepcion del destinatario');
		}
		$this->_previousStatus = $this->_status;
		$this->_status = App_Model_Correspondence::STATUS_WAITING;
		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($this);
		
		$history = new App_Model_CorrespondenceHistory($this, $this->getStatus());
		$history->save();
	}

	public function archive() {
		if ($this->_status !== App_Model_Correspondence::STATUS_RECEIVED && $this->_status !== App_Model_Correspondence::STATUS_RESENT && $this->_status !== App_Model_Correspondence::STATUS_SENT) {
			throw new Exception('No se puede archivar la correspondencia seleccionada');
		}
		$this->_previousStatus = $this->_status;
		$this->_status = App_Model_Correspondence::STATUS_ARCHIVED;
		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($this);
		
		$history = new App_Model_CorrespondenceHistory($this, $this->getStatus());
		$history->save();
	}

	public function delete() {
		if ($this->_status !== App_Model_Correspondence::STATUS_TO_SEND) {
			throw new Exception('No se puede preparar la recepcion del destinatario');
		}
		$this->_previousStatus = $this->_status;
		$this->_status = App_Model_Correspondence::STATUS_DELETED;
		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($this);
		
		$history = new App_Model_CorrespondenceHistory($this, $this->getStatus());
		$history->save();
	}

	public function finishReception() {
		if ($this->_status !== App_Model_Correspondence::STATUS_WAITING) {
			throw new Exception('No se puede finalizar la recepcion');
		}
		$this->_previousStatus = $this->_status;
		$this->_status = App_Model_Correspondence::STATUS_RECEIVED;
		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($this);
		
		$history = new App_Model_CorrespondenceHistory($this, $this->getStatus());
		$history->save();
	}

	public function startResend() {
		if ($this->_status !== App_Model_Correspondence::STATUS_RECEIVED && $this->_status !== App_Model_Correspondence::STATUS_RESENT) {
			throw new Exception('No se puede inicar el fordward');
		}
		$this->_previousStatus = $this->_status;
		$this->_status = App_Model_Correspondence::STATUS_TO_RESEND;
		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($this);
		
		$history = new App_Model_CorrespondenceHistory($this, $this->getStatus());
		$history->save();
	}
	
	public function cancelResend() {
		if ($this->_status !== App_Model_Correspondence::STATUS_TO_RESEND) {
			throw new Exception("No se puede cancelar el reenviar");
		}
		$this->_status = $this->_previousStatus;
		$dao = new App_Dao_CorrespondenceDao();
		$dao->save($this);
		
		$history = new App_Model_CorrespondenceHistory($this, $this->getStatus());
		$history->save();
	}

	public function getNaturalStatus() {
		$naturalStatus = "";
		switch($this->getStatus()) {
			case App_Model_Correspondence::STATUS_ARCHIVED :
				$naturalStatus = "Archivado";
				break;
			case App_Model_Correspondence::STATUS_DELETED :
				$naturalStatus = "Eliminado";
				break;
			case App_Model_Correspondence::STATUS_RECEIVED :
				$naturalStatus = "Recibido";
				break;
			case App_Model_Correspondence::STATUS_RESENT :
				$naturalStatus = "Reenviado";
				break;
			case App_Model_Correspondence::STATUS_SENT :
				$naturalStatus = "Enviado";
				break;
			case App_Model_Correspondence::STATUS_TO_RESEND :
				$naturalStatus = "Por Reenviar";
				break;
			case App_Model_Correspondence::STATUS_TO_SEND :
				$naturalStatus = "Por Enviar";
				break;
			case App_Model_Correspondence::STATUS_TO_WAIT :
				$naturalStatus = "Por Recibir";
				break;
			case App_Model_Correspondence::STATUS_WAITING :
				$naturalStatus = "Esperando";
				break;
			default:
				$naturalStatus = "Estado no reconocido";
		}
		return $naturalStatus;
	}

	public function toString() {
		$result = "{";
		$result .= "\"id\" : " . $this->_id . ",";
		$result .= "\"owner\" : " . $this->_owner . ",";
		$result .= "\"level\" : " . $this->_level . ",";
		$result .= "\"message\" : " . $this->_message . ",";
		$result .= "}";
		return $result;
	}
	
	public function getCorrespondenceHistoryNodeByStatus($status) {
		$foundNode = null;
		foreach($this->getCorrespondenceHistory() as $correspondenceHistoryNode) {
			/**
			 * @var App_Model_CorrespondenceHistory
			 */
			$correspondenceHistoryNode;
			
			if($correspondenceHistoryNode->getStatus() == $status) {
				$foundNode = $correspondenceHistoryNode;
			}
		}
		return $foundNode;
	}
	
	public static function getAll($limit, $offset) {
		$dao = new App_Dao_CorrespondenceDao();
		return $dao->getAll($limit, $offset);
	}
	
	public function getLastHistoryNode() {
		$count = count($this->_correspondenceHistory);
		return $this->_correspondenceHistory[$count-1];
	}
}
