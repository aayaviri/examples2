<?php

class CorrespondenceController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
		$this->view->activeMenuItem = "correspondence";
	}

	public function indexAction() {
		/* Index action controller */
		$auth = Zend_Auth::getInstance();
		$employee = App_Model_Employee::getByUserId($auth->getIdentity());
		$this->view->correspondenceList = array();
		if($employee != null) {
			$this->view->correspondenceList = App_Model_Correspondence::getAllReceivedAndToReceive($employee->getId());
		}
	}

	public function viewAction() {
		$id = $this->_getParam('id', '');
		$readOnly = $this->_getParam("readOnly", false);
		
		if (empty($id)) {
			$this->helper->redirector('index');
		}
		$this->view->correspondence = App_Model_Correspondence::getById($id);
		$this->view->readOnly = $readOnly;
	}

	public function externalAction() {
		$externalCorrespondenceList = App_Model_Correspondence::getAllExternalToReceive(100, 0);
		$this->view->externalCorrespondenceList = $externalCorrespondenceList;
	}

	public function addCorrespondenceAction() {
		if ($this->_request->getPost()) {
			$origin = $this->_getParam('origin', '');
			$formData = $this->_request->getPost();
			if ($origin === App_Model_Correspondence::ORIGIN_EXTERNAL) {
				$endPoint = App_Model_ExternalPerson::getById($formData['external-person']);
				$documentType = App_Model_DocumentType::getById($formData['document-type-external']);
			} elseif ($origin === App_Model_Correspondence::ORIGIN_INTERNAL) {
				$auth = Zend_Auth::getInstance();
				$sessUser = App_Model_User::getById($auth->getIdentity());
				$endPoint = $sessUser->getEmployee();
				if ($endPoint === null) {
					//TODO: Implementar mensaje de que no es empleado
					$this->_redirect("correspondence/sent");
				}
				$correspondenceType = $formData["correspondence-type"];
				if ($correspondenceType === App_Model_DocumentType::EXTERNAL) {
					$documentType = App_Model_DocumentType::getById($formData['document-type-external']);
				} else {
					$documentType = App_Model_DocumentType::getById($formData['document-type-internal']);
				}
			}
			$correspondence = new App_Model_Correspondence($endPoint, new App_Model_CorrespondenceBody($formData['subject'], $documentType), $origin);
			$correspondence->start();
			$this->_redirect("correspondence/view/id/" . $correspondence->getId());
		} else {
			$origin = $this->_getParam('origin', '');
			if (empty($origin)) {
				$this->_redirect($_SERVER["HTTP_REFERER"]);
			}

			if ($origin === App_Model_Correspondence::ORIGIN_EXTERNAL) {
				$this->view->origin = $origin;
				$this->view->externalPersonList = App_Model_ExternalPerson::getAll(100, 0);
				$this->view->documentTypeExternalList = App_Model_DocumentType::getAllExternal(100, 0);
			} elseif ($origin === App_Model_Correspondence::ORIGIN_INTERNAL) {
				$this->view->origin = $origin;
				$this->view->externalPersonList = App_Model_ExternalPerson::getAll(100, 0);
				$this->view->employeeList = App_Model_Employee::getAll(100, 0);
				$this->view->documentTypeExternalList = App_Model_DocumentType::getAllExternal(100, 0);
				$this->view->documentTypeInternalList = App_Model_DocumentType::getAllInternal(100, 0);
			} else {
				$this->_redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}

	public function editCorrespondenceAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$correspondence = App_Model_Correspondence::getById($formData["id"]);
			$correspondence->changeSubject($formData['subject']);
			if (array_key_exists("sender", $formData)) {
				$externalPerson = App_Model_ExternalPerson::getById($formData['sender']);
				$correspondence->changeOwner($externalPerson);
			}
			$correspondence->modify();
			$this->_redirect("correspondence/view/id/" . $correspondence->getId());
		} else {
			$id = $this->_getParam('id', '');
			if (empty($id))
				$this->_redirect($_SERVER["HTTP_REFERER"]);

			$correspondence = App_Model_Correspondence::getById($id);

			if ($correspondence->getDocumentType()->getType() === App_Model_DocumentType::EXTERNAL) {
				$this->view->type = App_Model_DocumentType::EXTERNAL;
				$this->view->externalPersonList = App_Model_ExternalPerson::getAll(100, 0);
			} elseif ($correspondence->getDocumentType()->getType() === App_Model_DocumentType::INTERNAL) {
				$this->view->type = App_Model_DocumentType::INTERNAL;
			} else {
				$this->_redirect($_SERVER['HTTP_REFERER']);
			}
			$this->view->correspondence = $correspondence;
		}
	}

	public function deleteAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_redirect($_SERVER["HTTP_REFERER"]);

		$correspondence = App_Model_Correspondence::getById($id);
		$correspondence->delete();
		$this->_redirect($_SERVER["HTTP_REFERER"]);
	}

	public function cancelToResendAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_redirect($_SERVER["HTTP_REFERER"]);

		$correspondence = App_Model_Correspondence::getById($id);
		$correspondence->cancelResend();
		$this->_redirect($_SERVER["HTTP_REFERER"]);
	}

	public function archiveAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_redirect($_SERVER["HTTP_REFERER"]);

		$correspondence = App_Model_Correspondence::getById($id);
		$correspondence->archive();
		$this->_redirect($_SERVER["HTTP_REFERER"]);
	}

	public function addReceiverAction() {
		//TODO:
		//1. Get all receivers list.
		//2. Get all posible actions to do
		//3. Assign selected Actions to do
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();

			if (array_key_exists("external-person-id", $formData)) {
				$receiver = App_Model_ExternalPerson::getById($formData['external-person-id']);
			} elseif (array_key_exists("employee-id", $formData)) {
				$receiver = App_Model_Employee::getById($formData['employee-id']);
			}

			$assignedActions = array();
			foreach ($formData['actions'] as $actionId) {
				$action = App_Model_Action::getById($actionId);
				$assignedActions[] = new App_Model_AssignedAction($action);
			}

			$correspondence = App_Model_Correspondence::getById($formData['correspondence-id']);
			$correspondence->addReceiver($receiver, $assignedActions, $formData["message"]);
			$this->redirect($formData["referer"]);
		} else {
			$correspondence = App_Model_Correspondence::getById($this->_getParam('id', ''));
			$this->view->correspondence = $correspondence;
			if ($correspondence->getOrigin() === App_Model_Correspondence::ORIGIN_EXTERNAL) {
				$this->view->employeeList = App_Model_Employee::getAllWithCompanyArea(100, 0);
				$this->view->actionList = App_Model_Action::getAll(100, 0);
			} elseif ($correspondence->getOrigin() === App_Model_Correspondence::ORIGIN_INTERNAL) {
				$this->view->employeeList = App_Model_Employee::getAllWithCompanyArea(100, 0);
				$this->view->externalPersonList = App_Model_ExternalPerson::getAll(100, 0);
				$this->view->actionList = App_Model_Action::getAll(100, 0);
			} else {
				$this->_redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}

	public function deleteReceiverAction() {
		$id = $this->_getParam("id", "");
		if (empty($id)) {
			$this->redirect($_SERVER["HTTP_REFERER"]);
		}

		$receiver = App_Model_Correspondence::getById($id);
		$receiver->remove();
		$this->redirect($_SERVER["HTTP_REFERER"]);
	}

	public function uploadAction() {
		if ($this->_request->isPost()) {

			$formData = $this->_request->getPost();

			/* Uploading Document File on Server */
			$tmpPath = APPLICATION_PATH . "/../public/file/correspondence/";
			$upload = new Zend_File_Transfer_Adapter_Http();
			$upload->setDestination($tmpPath);
			try {
				// upload received file(s)
				$upload->receive();
			} catch (Zend_File_Transfer_Exception $e) {
				$e->getMessage();
			}

			// you MUST use following functions for knowing about uploaded file
			# Returns the file name for 'doc_path' named file element
			$name = $upload->getFileName('upload-file');

			# Returns the size for 'doc_path' named file element
			# Switches of the SI notation to return plain numbers
			//$upload->setOption(array('useByteString' => false));
			$size = $upload->getFileSize('upload-file');

			# Returns the mimetype for the 'doc_path' form element
			$mimeType = $upload->getMimeType('upload-file');

			$correspondence = App_Model_Correspondence::getById($formData["id"]);

			// New Code For Zend Framework :: Rename Uploaded File
			$extension = substr($name, -4);
			$renameFile = $correspondence->getControlCode() . '.' . $extension;

			$fullFilePath = $tmpPath . $renameFile;

			$correspondence->changeFilePath($renameFile);

			// Rename uploaded file using Zend Framework
			$filterFileRename = new Zend_Filter_File_Rename( array(
				'target' => $fullFilePath,
				'overwrite' => true
			));

			$filterFileRename->filter($name);

			$this->_redirect($formData["referer"]);

		} else {
			$id = $this->_getParam("id", "");
			if (empty($id)) {
				$this->redirect($_SERVER["HTTP_REFERER"]);
			}
			$this->view->correspondence = App_Model_Correspondence::getById($id);
		}
	}

	public function sendAction() {
		$id = $this->_getParam("id", "");
		if (empty($id)) {
			$this->redirect($_SERVER["HTTP_REFERER"]);
		}
		$correspondence = App_Model_Correspondence::getById($id);
		$correspondence->send();
		$this->redirect($_SERVER["HTTP_REFERER"]);
	}

	public function endReceptionAction() {
		$id = $this->_getParam("id", "");
		if (empty($id)) {
			$this->redirect($_SERVER["HTTP_REFERER"]);
		}
		$correspondence = App_Model_Correspondence::getById($id);
		$correspondence->finishReception();
		$this->redirect($_SERVER["HTTP_REFERER"]);
	}

	public function startResendAction() {
		$id = $this->_getParam("id", "");
		if (empty($id)) {
			$this->redirect($_SERVER["HTTP_REFERER"]);
		}
		$correspondence = App_Model_Correspondence::getById($id);
		$correspondence->startResend();
		$this->redirect($_SERVER["HTTP_REFERER"]);
	}

	public function sentAction() {
		$auth = Zend_Auth::getInstance();
		$sessUser = App_Model_User::getById($auth->getIdentity());
		$this->view->sentCorrespondenceList = array();
		if ($sessUser->getEmployee() != null) {
			$this->view->sentCorrespondenceList = App_Model_Correspondence::getAllSent($sessUser->getEmployee()->getId());
		}
	}
	
	public function archivedAction() {
		$auth = Zend_Auth::getInstance();
		$sessUser = App_Model_User::getById($auth->getIdentity());
		$this->view->archivedCorrespondenceList = array();
		if ($sessUser->getEmployee() != null) {
			$this->view->archivedCorrespondenceList = App_Model_Correspondence::getAllArchived($sessUser->getEmployee()->getId());
		}
	}

	public function historyAction() {
		$id = $this->_getParam("id", "");
		if (empty($id)) {
			$this->redirect($_SERVER["HTTP_REFERER"]);
		}

		$correspondence = App_Model_Correspondence::getById($id);

		if ($correspondence->getSender() != null) {
			$senderId = $correspondence->getSender()->getId();
		} else {
			$senderId = "";
		}

		if ($correspondence->getOwner() instanceof App_Model_Employee) {
			if ($correspondence->getOwner()->getCompanyArea() != null) {
				$rank = $correspondence->getOwner()->getCompanyArea()->getCode();
			} else {
				$rank = "";
			}
		} else {
			if ($correspondence->getOwner()->getCompanyName() != null) {
				$rank = $correspondence->getOwner()->getCompanyName();
			} else {
				$rank = "";
			}
		}

		$historyArray = array();
		$historyArray[] = array(
			"correspondenceId" => $correspondence->getId(),
			"ownerName" => $correspondence->getOwner()->getFullName(),
			"controlCode" => $correspondence->getControlCode(),
			"rank" => $rank,
			"senderId" => $senderId
		);

		$current = $correspondence;
		while ($current->getSender() != null) {
			$sender = $current->getSender();
			if ($sender->getSender() != null) {
				$senderId = $sender->getSender()->getId();
			} else {
				$senderId = "";
			}

			if ($sender->getOwner() instanceof App_Model_Employee) {
				if ($sender->getOwner()->getCompanyArea() != null) {
					$rank = $sender->getOwner()->getCompanyArea()->getName();
				} else {
					$rank = "";
				}
			} else {
				if ($sender->getOwner()->getCompanyName() != null) {
					$rank = $sender->getOwner()->getCompanyName();
				} else {
					$rank = "";
				}
			}

			$historyArray[] = array(
				"correspondenceId" => $sender->getId(),
				"ownerName" => $sender->getOwner()->getFullName(),
				"controlCode" => $correspondence->getControlCode(),
				"rank" => $rank,
				"senderId" => $senderId
			);
			$current = $sender;
		}

		$this->view->historyArray = array_reverse($historyArray);
		$this->view->correspondence = $correspondence;
	}

	public function pathPageAction() {
		$this->_helper->layout->setLayout('clean-layout');
		
		$id = $this->_getParam("id", "");
		$correspondence = App_Model_Correspondence::getById($id);
		$this->view->correspondence = $correspondence;
	}
	
	public function allAction() {
		$this->view->correspondenceList = App_Model_Correspondence::getAll(1000,0);
	}
}
